localStorage.setItem("lastVisitedUrl", window.location.href);

var btnLogin = document.getElementById('btn-login');
var btnLogout = document.getElementById('btn-logout');

initButtons();


btnLogin.addEventListener('click', function (e) {
  e.preventDefault();
  lock.show();
})

btnLogout.addEventListener('click', function (e) {
  logout();
})


function logout() {
  localStorage.clear();
  Cookies.remove('_rid');
  Cookies.remove('_accessToken');
}

function initButtons() {
  if (Cookies.get('_rid')) {
    switchToLogoutBtn();
  } else {
    switchToLoginBtn();
  }
}

function switchToLoginBtn() {
  btnLogin.style.display = 'inline-block';
  btnLogout.style.display = 'none';

}

function switchToLogoutBtn() {
  btnLogin.style.display = 'none';
  btnLogout.style.display = 'inline-block';

}
